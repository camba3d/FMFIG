a1 = [ 3 -0.1 -0.2; 0.1 7 -0.3; 0.3 -0.2 10 ];
b1 = [ 7.85; -19.3; 71.4 ];

a2 = [ 5 2 -1 1; 1 7 3 -1; -1 4 9 2; 1 -1 1 4 ];
b2 = [ 12; 2; 1; 3 ];

E = 0.1; % tolerancia
M = 5; % maximo iteraciones.

% Ejercicio 1.
disp("GAUSS 1: ");
display(gauss(a1,b1,M,E));
disp("JACOBI 1: ");
display(jacobi(a1,b1,M,E));

% Ejercicio 2.
disp("GAUSS 2: ");
display(gauss(a2,b2,M,E));
disp("JACOBI 2: ");
display(jacobi(a2,b2,M,E));



% GAUSS SEIDEL
function X = gauss(A,B,M,E)

    % VARS.
    n = length(B); % numero de ecuaciones.
    X0 = zeros(1,n); % vector anterior.
    X = X0; % vector actual.
    K = 0; % control de iteraciones
    Delta = 1; % control de error.
    
    % BODY.
    while Delta > E && K < M
        K = K+1;
        
        % Iteración.
        for i = 1:n
            suma = 0;
            
            % Sumatorio.
            for j = 1:n
                if i~=j
                    suma = suma + A(i,j) * X(j);
                end
            end % for j = 1:n
            
            %Formula.
            X(i) = (B(i) - suma) / A(i,i);
            
        end % for i = 1:n
        
        % Condición de parada.
        Delta = norm(X0 - X);
        
        % Volcado auxiliar.
        X0 = X;
        
    end % while Delta > E && K < M
end % function X = gauss(A,B,M,E)


% JACOBI !
function X = jacobi(A,B,M,E)

    % VARS.
    n = length(B); % numero de ecuaciones.
    X0 = zeros(1,n); % vector anterior.
    X = X0; % vector actual.
    K = 0; % control de iteraciones
    Delta = 1; % control de error.
    
    % BODY.
    while Delta > E && K < M
        K = K+1;
        
        % Iteración.
        for i = 1:n
            suma = 0;
            
            % Sumatorio.
            for j = 1:n
                if i~=j
                    suma = suma + A(i,j) * X0(j);
                end
            end % for j = 1:n
            
            %Formula.
            X(i) = (B(i) - suma) / A(i,i);
            
        end % for i = 1:n
        
        % Condición de parada.
        Delta = norm(X0 - X);
        
        % Volcado auxiliar.
        X0 = X;
        
    end % while Delta > E && K < M
end % function X = jacobi(A,B,M,E)

