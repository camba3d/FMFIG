/*
 * This code is written as example for the FMFIG class of the
 * Master Universitario en Informatica Grafica, Juegos y Realidad Virtual.
 * Its purpose is to be didactic and easy to understand, not hard optimized.
 *
 * This file compute some statistic about a mesh and dump it to the console.
 *
 * //TODO: Fill-in your name and email
 * Name of alumn: Daniel Camba Lamas
 * Email of alumn: <cambalamas@gmail.com>
 * Year: 2017
 *
 */

#define _CRT_NONSTDC_NO_DEPRECATE
#include <ColorMesh3.hpp>
#include <SimpleMesh3.hpp>
#include <iostream>

// DBL_MAX, DBL_MIN.
#include <cfloat>

// Functions definition.
int meshlab(ColorMesh m);
std::vector<double> voronoi(ColorMesh m);
double cosOf(vec3 v0, vec3 v1, vec3 v2);
double angleOf(vec3 v0, vec3 v1, vec3 v2);
double cotanOf(vec3 v0, vec3 v1, vec3 v2);

int main(int argc, char* argv[])
{

#ifdef _OPENMP
    cout << "Compiled with OpenMP support" << endl;
#endif

    try {
        // Set input mesh filename
        std::string filename("mallas/mask2.off");
        if (argc > 1)
            filename = std::string(argv[1]);

        ///////////////////////////////////////////////////////////////////////
        //Read a mesh from given filename
        ColorMesh mesh1;
        cout << "Loading file " << filename << endl;
        mesh1.readFile(filename, false);

        cout << "Num vertex: " << mesh1.numVertex() << " Num triangles: " << mesh1.numTriangles()
             << " Unreferenced vertex: " << mesh1.checkUnreferencedVertex() << endl;

        //Compute minimum, maximum and average area per triangle, and total area for the mesh
        double minArea, maxArea, averageArea, totalArea;

        //Compute minimum and maximum angle for the mesh
        double minAngle, maxAngle;

        //////////////////////////////////////////////////////////////////////////////////
        //TODO 1.1:
        // Compute the values for minArea, maxArea, averageArea, totalArea
        // Compute the values for minAngle, maxAngle;

        // Init variables.
        minArea = minAngle = DBL_MAX;
        maxArea = maxAngle = averageArea = totalArea = DBL_MIN;

        // LAMBDA : To compute angles.
        auto lambdaAngle = [&](SimpleTriangle t, int zeroIndex) {
            vec3 v0, v1, v2;
            switch (zeroIndex) {
            case 0:
                v0 = mesh1.coordinates[t.a];
                v1 = mesh1.coordinates[t.b];
                v2 = mesh1.coordinates[t.c];
                break;
            case 1:
                v0 = mesh1.coordinates[t.b];
                v1 = mesh1.coordinates[t.a];
                v2 = mesh1.coordinates[t.c];
                break;
            case 2:
                v0 = mesh1.coordinates[t.c];
                v1 = mesh1.coordinates[t.b];
                v2 = mesh1.coordinates[t.a];
                break;
            default:
                cout << "Unreachable." << endl;
                break;
            }
            return angleOf(v0, v1, v2);
        };

        // Iterate mesh triangles.
        for (size_t i = 0; i < mesh1.triangles.size(); i++) {
            // Current triangle.
            SimpleTriangle t = mesh1.triangles[i];
            // AREAs
            double currArea = mesh1.triangleArea(t);
            if (currArea > maxArea)
                maxArea = currArea;
            if (currArea < minArea)
                minArea = currArea;
            totalArea += currArea;
            // ANGLEs
            for (int angle = 0; angle <= 2; angle++) {
                double currAngle = lambdaAngle(t, angle);
                if (currAngle > maxAngle)
                    maxAngle = currAngle;
                if (currAngle < minAngle)
                    minAngle = currAngle;
            }
        }
        //The total area between the total triangles.
        averageArea = totalArea / mesh1.numTriangles();
        //END TODO 1.1

        cout << "minArea: " << minArea << " maxArea: " << maxArea << " averageArea: " << averageArea << " totalArea: " << totalArea << endl;
        cout << "minAngle: " << minAngle << " maxAngle: " << maxAngle << endl;

        //////////////////////////////////////////////////////////////////////////////////
        //TODO OPTATIVE 1:
        //Compute Vertex area for each vertex and store in vertexAreas

        std::vector<double> vertexAreas(mesh1.numVertex(), 0.0);
        auto obtuses = mesh1.checkObtuseTriangles2();

        for (size_t i = 0; i < mesh1.triangles.size(); i++) {
            // Curr triangle.
            SimpleTriangle t = mesh1.triangles[i];
            // Curr vertexs.
            vec3 A = mesh1.coordinates[t.a];
            vec3 B = mesh1.coordinates[t.b];
            vec3 C = mesh1.coordinates[t.c];
            // Compute...
            if (obtuses[i]) {
                // Curr angles.
                auto cosA = cosOf(A, B, C);
                auto cosB = cosOf(B, A, C);
                auto cosC = cosOf(C, A, B);
                // Check angle A.
                if (cosA < 0.0)
                    vertexAreas[t.a] += mesh1.triangleArea(t) / 2.0;
                else
                    vertexAreas[t.a] += mesh1.triangleArea(t) / 4.0;
                // Check angle B.
                if (cosB < 0.0)
                    vertexAreas[t.b] += mesh1.triangleArea(t) / 2.0;
                else
                    vertexAreas[t.b] += mesh1.triangleArea(t) / 4.0;
                // Check angle C.
                if (cosC < 0.0)
                    vertexAreas[t.c] += mesh1.triangleArea(t) / 2.0;
                else
                    vertexAreas[t.c] += mesh1.triangleArea(t) / 4.0;
            } else {
                // Curr cotans.
                double cotA = cotanOf(A, B, C);
                double cotB = cotanOf(B, A, C);
                double cotC = cotanOf(C, A, B);
                // Curr edges modules.
                double edgeBC = (C - B).module2();
                double edgeAC = (C - A).module2();
                double edgeAB = (B - A).module2();
                // Vornoi partial.
                auto voronoiA = cotA * edgeBC / 8.0;
                auto voronoiB = cotB * edgeAC / 8.0;
                auto voronoiC = cotC * edgeAB / 8.0;
                // Storage.
                vertexAreas[t.a] += voronoiB + voronoiC;
                vertexAreas[t.b] += voronoiA + voronoiC;
                vertexAreas[t.c] += voronoiA + voronoiB;
            }
        }

        //END TODO OPTATIVE 1

        //Check values for vertex areas and compute sum of areas:
        double sumAreas = 0.0;
        for (double s : vertexAreas) {
            sumAreas += s;
            if (s < 0) {
                cout << "Negative VertexArea " << s << endl;
                throw "Invalid VertexArea";
            }
        }

        if (sumAreas > 0.0)
            cout << "OPTATIVE: Sum of VertexAreas = " << sumAreas << endl;

        //////////////////////////////////////////////////////////////////////////////////
        //TODO 1.2:
        //Write faces with ShapeFactor (radius / minEdge) greater than shapeFactorTh
        //Use messages as: "Triangle nnn has ShapeFactor xxx"
        double shapeFactorTh = 4.0;
        std::vector<SimpleTriangle> overShapeFactor;
        cout << "List of triangles with ShapeFactor > " << shapeFactorTh << endl;
        for (size_t i = 0; i < mesh1.triangles.size(); i++) {
            // Current triangle.
            SimpleTriangle t = mesh1.triangles[i];
            auto sf = mesh1.triangleShapeFactor1(t);
            if (sf > shapeFactorTh) {
                cout << "Triangle " << i << " has ShapeFactor " << sf << endl;
                // Save "t" for 1.3 exercise.
                overShapeFactor.push_back(t);
            }
        }
        //END TODO 1.2

        //////////////////////////////////////////////////////////////////////////////////
        //TODO 1.3:
        //Create a colorMesh where faces with ShapeFactor greater than shapeFactorTh
        //have their vertex colored in red. Save it to file named output_meshStatistic.obj
        //and visualize it with meshlab or ant other external viewer.
        for (size_t i = 0; i < overShapeFactor.size(); i++) {
            SimpleTriangle t = overShapeFactor[i];
            mesh1.colors[t.a].set(1, 0, 0);
            mesh1.colors[t.b].set(1, 0, 0);
            mesh1.colors[t.c].set(1, 0, 0);
        }
        return meshlab(mesh1);
        //END TODO 1.3

    }

    catch (const string& str) {
        std::cerr << "EXCEPTION: " << str << std::endl;
    } catch (const char* str) {
        std::cerr << "EXCEPTION: " << str << std::endl;
    } catch (std::exception& e) {
        std::cerr << "EXCEPTION: " << e.what() << std::endl;
    } catch (...) {
        std::cerr << "EXCEPTION (unknow)" << std::endl;
    }

#ifdef WIN32
    cout << "Press Return to end the program" << endl;
    cin.get();
#else
#endif

    return 0;
}

// --- HELPERS

// Compute the angle in "v0".
double angleOf(vec3 v0, vec3 v1, vec3 v2)
{
    // Triangle vectors.
    vec3 e1 = v1 - v0;
    vec3 e2 = v2 - v0;
    // |e1|·|e2|
    double e1e2 = sqrt((e1 * e1) * (e2 * e2));
    double cosine = (e1 * e2) / e1e2;
    double sine = (e1 ^ e2).module() / e1e2;
    return atan2(sine, cosine);
}

// Compute the cos of "v0".
double cosOf(vec3 v0, vec3 v1, vec3 v2)
{
    // Triangle vectors.
    vec3 e1 = v1 - v0;
    vec3 e2 = v2 - v0;
    return e1 * e2;
}

// Compute de cotan of the angle in "v0".
double cotanOf(vec3 v0, vec3 v1, vec3 v2)
{
    //Compute edges
    vec3 e_ab = v1 - v0;
    vec3 e_ac = v2 - v0;
    //Compute sin and cos with cross and dot product
    double sin_A = (e_ab ^ e_ac).module();
    double cos_A = e_ab * e_ac;
    //Compute cotangent of angle in vertex va
    double cot_A = cos_A / sin_A;
    return cot_A;
}

// Open in meshlab !
int meshlab(ColorMesh m)
{
    // Save result to a file in .ply format
    string objFilename = "output_meshStatistic.obj";
    cout << "Saving output to " << objFilename << endl;
    m.writeFileOBJ(objFilename);

#ifdef WIN32
    string viewcmd = "meshlab.exe";
#else
    string viewcmd = "meshlab";
#endif

    // Visualize the .ply file with an external viewer
    string cmd = viewcmd + " " + objFilename;
    cout << "Executing external command: " << cmd << endl;
    return system(cmd.c_str());
}
